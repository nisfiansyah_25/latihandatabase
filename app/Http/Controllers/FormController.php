<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function form(){
        return view("halaman.biodata"); 
    }
    public function kirim(request $request){
        // dd($request->all());
        $fname = $request->fname;
        $lname = $request->lname;        
        return view("halaman.home", compact('fname','lname'));
    }
}
