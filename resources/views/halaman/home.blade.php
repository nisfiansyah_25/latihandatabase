@extends('layout.master')

@section('judul')
    Welcome
@endsection

@section('content')
        <h1>Selamat Datang! {{$fname}} {{$lname}}</h1>
        <h3>Terima kasih telah bergabung di Website kami. Media Belajar kita bersama!</h3>
@endsection